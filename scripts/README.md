## Build the docker images locally


If you forked the repository on GitLab, you can use:

```sh
./scripts/create_docker_image.sh registry.gitlab.com/<your_gitlab_username>/opam-repository
```
